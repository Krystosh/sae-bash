Pourquoi un dual-boot ?
	J'ai effectuer un dual boot car je vais bientôt acheter un nouvelle ordinateur, et je préfère consacré
celui-ci à mon travail, bien que les autres options soit stable, je préfère un environnement de travail un peu
plus concrêt et ressemblant à celui de l'IUT.
Afin de parvenir au même travail que moi j'ai du me munir de :
	-Clés USB 
	-Image ISO d'Ubuntu 
	-Rufus(non obligatoire)
Afin d'avoir l'image ISO d'Ubuntu, je me suis rendu sur le site officiel d'Ubuntu et simplement télécharger
la dernière version LTS (Long Term Support). Suite à cela installer Rufus si on le souhaite.
Lorsque tous cela est prêt il suffit d'allez sur Rufus, selectionner le périphérique et sélectionner l'ISO 
qui est celui qu'on à installer. (Le formatage de la clés USB se fait automatiquement via Rufus) Ensuite il suffit de
mettre démarrer et attendre !
Lorsque cela est fini il faut donc mettre la clés dans le pc cible puis lancer le BIOS, une fois dans le BIOS,
il faut rechercher 'option d'amrocage' ou quelques chose de similaire au boot. Par la suite rechercher l'ordre de 
démarrage et mettre la clés USB en premier ! (+ ou - pour déplacer enrengistrer et redémarrer le pc)
Si tous se passe bien le pc vous redirigera automatiquement sur votre clés USB.
Première page : Langue + choix essayer ou installer, cocher installer !
Deuxième page : Choix de la disposition du clavier
Troisème page : Connexion à internet (non obligatoire)
Quatrième page : Installation en plus (au choix, de préférence normal) et cocher (installer un logiciel tier pour le
matériel graphique et Wi-Fi ...
Cinquème page :  Type d'installation (Dual boot ou supprimer Windows)
Sixième page : Le partitionnement, selectionner le disque ou l'on veut télécharger Ubuntu et choisir la place 
souhaiter dans le disque (50Go suffisant) et clicker sur installer maintenant !
Plus quelques infos simple possible à effectuer sans aide (fuseau horaire, nom, prénom...)


Install :
	-Java :
		- sudo apt install openjdk-11-jdk-headless
	-Docker :
		- sudo apt update
		- sudo apt install apt-transport-https ca-certificates curl software-properties-common
		- curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
		- sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
		- sudo apt update
		- apt-cache policy docker-ce
		- sudo apt install docker-ce
		- sudo systemctl status docker
	-VsCode :
		- sudo snap install code --classic
		- code --install-extension ms-python.python
		- code --install-extension njpwerner.autodocstring
		- pip install -U pytest
		- sudo apt install python3-pytest

Mon git : https://gitlab.com/Krystosh/sae-bash.git
